Grouping menu items Module
------------------------
Written by Ruslan Telyak.


Description
-----------
Grouping menu items is Drupal, which provides consolidation of several links
into one menu.

Features
--------
  - User can create a new menu item and place either " <group>" to the Path
  field, without quotes.
  - Add "<gruop>" children and all these links will be merged into a
  single items.

Installation
------------
1. Copy the grouping_menu_items folder to your sites/all/modules directory.
2. At Administer -> Site building -> Modules (admin/modules) enable the module.
3. Add menu item <group> path and add to this menu item children.
